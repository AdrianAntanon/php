<?php
//Ejercicio 1

echo '<strong>Ejercicio 1</strong>'.'<br>';
$a= 2;
$b= 3;
$suma= $b.$a+$b;
$operacion= $b*($a+$b);
echo 'La suma es: '.$suma;
echo '<br>'.'La operaci&oacute;n es: '.$operacion.'<br>';

//Ejercicio 2
echo '<br>'.'<strong>Ejercicio 2</strong>';
$valor= 11;
if($valor > 5 && $valor < 10){
    echo '<br>'.'El '.$valor. ' es mayor a 5 y menor que 10';
}else{
    echo '<br>'.'No cumple la condici&oacute;n';
};

if($valor >= 0 && $valor <= 20){
    echo '<br>'.'El '.$valor. ' es mayor o igual a 0 y menor o igual a 20';
}else{
    echo '<br>'.'No cumple la condici&oacute;n';
};

if($valor === '10'){
    echo '<br>'.'El '.$valor. ' es igual a 10 y es una cadena';
}else{
    echo '<br>'.'No cumple la condici&oacute;n';
};

if($valor > 0 && $valor < 5 || $valor > 10 && $valor < 15){
    echo '<br>'.'El '.$valor. ' es mayor a 0 pero menor a 5 o mayor a 10 pero menor que 15';
}else{
    echo '<br>'.'No cumple la condici&oacute;n';
};
 ?>
