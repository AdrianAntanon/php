<?php

$arreglo = [
    'keyStr1' => 'lado',
    0 => 'ledo',

    'keyStr2' => 'lido',
    1 => 'lodo',
    2 => 'ludo'

];

echo '<strong> Ejercicio 1: <br></strong>';

echo $arreglo['keyStr1'] . ', ' . $arreglo[0] . ', ' . $arreglo['keyStr2'] . ', ' . $arreglo[1] . ', ' . $arreglo[2];

echo '<br>' . 'decirlo al rev&eacute;s lo dudo.' . '<br>';

echo $arreglo[2] . ', ' . $arreglo[1] . ', ' . $arreglo['keyStr2'] . ', ' . $arreglo[0] . ', ' . $arreglo['keyStr1'];

echo '<br>' . '¡Qu&eacute; trabajo me ha costado!' . '<br>';


$paises = [
    'Ecuador' => ['Quito', 'Guayaquil', 'Cuenca'],
    'Mexico' => ['Monterrey', 'Quer&eacute;taro', 'Guadalajara'],
    'Colombia' => ['Bogot&aacute;', 'Cartagena', 'Medell&iacute;n'],
    'Japon' => ['Kioto', 'Tokio', 'Osaka'],
    'Spain' => ['Madrid', 'Barcelona', 'Murcia']
];

echo '<strong><br> Ejercicio 2: </strong>';
foreach ($paises as $pais => $ciudades) {
    echo utf8_decode('<strong><br>' . $pais . ': </strong>');
    $pais++;
    foreach ($ciudades as $ciudad) {
        echo utf8_decode($ciudad . ' ');
    };
};

$valores = [23, 54, 32, 67, 34, 78, 98, 56, 21, 34, 57, 92, 12, 5, 61];
$cant = count($valores) - 1;
sort($valores);

$max = [
    $valores[$cant],
    $valores[$cant - 1],
    $valores[$cant - 2]
];

$min = [
    $valores[0],
    $valores[1],
    $valores[2]
];

echo '<br>';
echo '<strong><br> Ejercicio 3: </strong>';
echo utf8_decode('<strong><br> N&uacute;meros Mayores </strong>');
foreach ($max as $numMax) {
    echo '<br>' . $numMax;
}

echo utf8_decode('<strong><br> N&uacute;meros Menores </strong>');
foreach ($min as $numMin) {
    echo '<br>' . $numMin;
}

echo '<br>' . '<strong>Utilizaci&oacute;n de forEach</strong>';

$_colors = [
    ["red", "blues", "black"],
    ["green", "yellow"]
];

$_colors = [["red", "blues", "black"], ["green", "yellow"]];
foreach ($_colors as $indice => $conjunto) {
    foreach ($_colors[$indice] as $key => $color) {
        echo "<li>$color</li>";
    }
}
