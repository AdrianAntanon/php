<?php
//Crea una clase Fecha. La clase contendrá además de constructores, métodos set y get y el método mostrar,
//un método para comprobar si la fecha es correcta
//y otro para modificar la fecha actual por la del día siguiente.

class CurrentDate{
    private $currentDate;
    public function __construct()
    {
        //obtengo un array con los parámetros enviados a la función
        $params = func_get_args();
        //saco el número de parámetros que estoy recibiendo
        $num_params = func_num_args();
        //cada constructor de un número dado de parámtros tendrá un nombre de función
        //atendiendo al siguiente modelo __construct1() __construct2()...
        $functionConstructor ='__construct'.$num_params;
        //compruebo si hay un constructor con ese número de parámetros
        if (method_exists($this,$functionConstructor)) {
            //si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
            call_user_func_array(array($this,$functionConstructor),$params);
        }
    }

    public function __construct0(){
        date_default_timezone_get("Europe/Berlin");
        $this->currentDate = 'La fecha es el ' . date("d.M.Y");
    }


    function getCurrentDate()
    {
        // echo "La fecha es " . date("M.D.Y");
        return $this->currentDate;
    }

    function setChangeCurrenDate($hour, $minutes, $seconds,  $month, $day, $year){
        $newDate =  mktime($hour, $minutes, $seconds, $month, $day, $year);
        $this->currentDate='La fecha es el ' . date("d.M.Y", $newDate);
    }

}


$newDate = new CurrentDate();

echo $newDate->getCurrentDate();

$newDate->setChangeCurrenDate(21, 30, 30, 5, 8, 2015);

echo $newDate->getCurrentDate();

