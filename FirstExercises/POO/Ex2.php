<?php

//Crea una clase Libro con los métodos préstamo, devolución y mostrar.
//La clase contendrá un constructor por defecto, un constructor con parámetros y los métodos getters y setters.

class Book{
    private $disponibility;
    private $name;
    private $author;


    public function __construct()
    {
        //obtengo un array con los parámetros enviados a la función
        $params = func_get_args();
        //saco el número de parámetros que estoy recibiendo
        $num_params = func_num_args();
        //cada constructor de un número dado de parámtros tendrá un nombre de función
        //atendiendo al siguiente modelo __construct1() __construct2()...
        $functionConstructor ='__construct'.$num_params;
        //compruebo si hay un constructor con ese número de parámetros
        if (method_exists($this,$functionConstructor)) {
            //si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
            call_user_func_array(array($this,$functionConstructor),$params);
        }
    }
    public function __construct0(){
        $this->__construct2("Unknown book", "Unknown author");
    }

    public function __construct2($name, $author){

        $this->author = $author;
        $this->name = $name;
        $this->disponibility = true;

    }

    function getName(){
        return $this->name;
    }

    function getAuthor(){
        return $this->author;
    }

    function setName($name){
        $this->name = $name;
    }

    function setAuthor($author){
        $this->author = $author;
    }

    function loanBook(){
        $this->disponibility = false;
    }

    function returnBook(){
        $this->disponibility = true;
    }

    function showBook(){

        $isAvailable = $this->disponibility ? "disponible" : "no disponible";
        return "El libro es " . $this->name . " y su autor/a es " . $this->author . " y ahora mismo está " . $isAvailable;
    }
}

$lordOfTheRings = new Book('El señor de los anillos', 'Tolkien');

echo $lordOfTheRings->showBook();

$lordOfTheRings->setAuthor("Patrick Rothfuss");
echo '<br/>';
echo $lordOfTheRings->getAuthor();
echo '<br/>';

$lordOfTheRings->setName("El Nombre del Viento");
echo $lordOfTheRings->getName();
echo '<br/>';

$unknown = new Book();

echo $unknown->showBook();
echo '<br/>';

echo $lordOfTheRings->showBook();
