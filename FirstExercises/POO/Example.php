<?php

class MyClass{
    public $public = 'Público';
    protected $protected = 'Protegido';
    private $private = 'Privado';


    public function __construct(){

    }

    function printAttributes(){ // Si no ponemos nada, por defecto es público
        echo $this ->public;
        echo $this ->private;
        echo $this ->protected;
    }
}

$obj = new MyClass();
echo $obj ->public;
//echo $obj ->private; Da error al ser private
//echo $obj ->protected; Da error al ser protected


class MyClass2 extends MyClass{
    public $public = 'Público 2';
    protected $protected = 'Protegido 2';

    public function __construct()
    {
//        parent->__construct();
    }

    function printAttributes(){ // Si no ponemos nada, por defecto es público
        echo $this ->public;
//        echo $this ->private; Da error al acceder a una variable privada del padre
        echo $this ->protected;
    }

}

abstract class Animal{
    protected function walk(){
        echo 'Caminando por la vida';
    }

    abstract function sound();
}

class Cat extends Animal{

    function sound()
    {
        echo 'Miauu';
    }
}

$cat = new Cat();

$cat->sound();

interface InterfaceTest{
    public function methodOne($attribute);
}

class MyClass3 implements InterfaceTest{

    public function methodOne($attribute)
    {
        echo $attribute;
    }
}