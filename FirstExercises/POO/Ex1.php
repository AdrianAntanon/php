<?php

//Crea una clase Contador con los métodos para incrementar y decrementar el contador.
//La clase contendrá un constructor por defecto, un constructor con parámetros y los métodos getters y setters.

class Counter
{
    private $counter;

    public function __construct()
    {
        //obtengo un array con los parámetros enviados a la función
        $params = func_get_args();
        //saco el número de parámetros que estoy recibiendo
        $num_params = func_num_args();
        //cada constructor de un número dado de parámtros tendrá un nombre de función
		//atendiendo al siguiente modelo __construct1() __construct2()...
		$functionConstructor ='__construct'.$num_params;
		//compruebo si hay un constructor con ese número de parámetros
		if (method_exists($this,$functionConstructor)) {
            //si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
            call_user_func_array(array($this,$functionConstructor),$params);
        }
    }

    public function __construct0(){
        $this->__construct1(0);
    }
    public function __construct1($counter)
    {
        $this->counter = $counter;
    }

    public function setCounter($counter)
    {
        $this->counter = $counter;
    }

    public function getCounter()
    {
        return $this->counter;
    }
}

echo 'Clase Counter con constructor por defecto<br/>';

$firstCounter = new Counter();

echo "Si hago un getCounter() me devuelve " . $firstCounter->getCounter() . ", ya que se establece en 0<br/>";

echo 'Clase Counter con constructor con parámetros<br/>';

$secondCounter = new Counter(10);

echo "Si hago un getCounter() me devuelve " . $secondCounter->getCounter() . ", ya que le pasado que tiene 10 en el constructor<br/>";
$secondCounter->setCounter(-5);
echo "Si hago un setCounter() me devuelve ahora " . $secondCounter->getCounter() . ", ya que lo he cambiado con el setter<br/>";

