<?php

$num = 10;

$factorial = calculateFactorial($num);

echo "El factorial de $num es $factorial";


function calculateFactorial($num){ 
    $factorial = 1; 
    for ($i = 1; $i <= $num; $i++){ 
      $factorial = $factorial * $i; 
    } 
    return $factorial; 
} 