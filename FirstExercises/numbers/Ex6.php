<?php

$word = "somos";
$anotherWord = "lunes";

checkPalindrome($word);
checkPalindrome($anotherWord);

function checkPalindrome($originalWord)
{
    $reverseWord = strrev($originalWord);

    if (strcmp($originalWord, $reverseWord) === 0) {
        echo "La cadena $originalWord es un palíndromo\n
        ";
    } else {
        echo "La cadena $originalWord no es un palíndromo\n
        ";
    }
}

$num = 1234567;
$anotherNum = 2222222;

checkPalindromic($num);
checkPalindromic($anotherNum);

function checkPalindromic($number)
{
    $reverseNumber = 0;
    $copyOfNumber = $number;

    while($copyOfNumber != 0){
        $remainder = (int)($copyOfNumber % 10);
        $reverseNumber = $reverseNumber * 10 + $remainder;
        $copyOfNumber = (int)($copyOfNumber / 10);
    }

    if ($number === $reverseNumber){
        echo "El número $number es capicúa\n
        ";
    }else{
        echo "El número $number no es capicúa\n
        ";
    }
}
