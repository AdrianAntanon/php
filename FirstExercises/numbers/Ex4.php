<?php

$numberToEvaluate = 5;
$perfectNumber = calculatePerfectNum($numberToEvaluate);
echo "El número $numberToEvaluate ";
if ($perfectNumber === 1) {
    echo "es un número perfecto";
} else {
    echo " no es un número perfecto";
}



function calculatePerfectNum($num)
{
    $count = 0;
    for ($i = 1; $i < $num; $i++) {
        if ($num % $i == 0) {
            $count = $count + $i;
        }
    }

    if ($count == $num) {
        return 1;
    } else {
        return 0;
    }
}
