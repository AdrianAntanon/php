<?php

$cars = ["uno","dos","tres"];

print_r($cars);
echo $cars[0] . '<br/>';
echo count($cars) . '<br/>';

for ($i=0;$i<count($cars);$i++){
    echo $cars. '<br/>';

}

//Array asociativos
$edad = array("Pedro" => "35", "Maria" => "21");

echo $edad["Pedro"] . '<br/>';

//Array multidimensional

$coches = array(
  array("Hola", 1, 2),
  array("Mundo", 3, 4)
);

echo $coches[0][0];


