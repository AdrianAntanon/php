<?php

function arrayStringSorted($array){
    for ($i=0;$i<count($array);$i++){
        for ($j=0;$j<count($array);$j++){
            if (strlen($array[$i])<strlen($array[$j])){
                $aux = $array[$i];
                $array[$i] = $array[$j];
                $array[$j] = $aux;
            }
        }
    }

    return $array;
}

$array = ['Hola', 'Mundo', 'Título', 'Sí'];

echo "Array desordenado ";
for ($i=0;$i<count($array);$i++){
    echo $array[$i] . " ";
}

$array = arrayStringSorted($array);

echo "<br/> Array ordenado ";

for ($i=0;$i<count($array);$i++){
    echo $array[$i] . " ";
}