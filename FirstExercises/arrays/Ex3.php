<?php

$array = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];

$copyArray  = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
];

echo "Array original ";

echo '<br/>';
for ($i=0;$i<count($array);$i++){
    for ($j=0;$j<count($array[$i]);$j++){
        echo $array[$i][$j] . ' ';
    }
    echo '<br/>';
}

for ($i=0;$i<count($array);$i++){
    for ($j=0;$j<count($array[$i]);$j++){
        $copyArray[$j][$i] = $array[$i][$j];
    }
}

echo 'Array transpuesto <br/>';


for ($i=0;$i<count($array);$i++){
    for ($j=0;$j<count($array[$i]);$j++){
        echo $copyArray[$i][$j] . ' ';
    }
    echo '<br/>';
}