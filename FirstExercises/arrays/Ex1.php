<?php

function arraySorted($array){
    for ($i=0;$i<count($array);$i++){
        for ($j=0;$j<count($array);$j++){
            if ($array[$i]<$array[$j]){
                $aux = $array[$i];
                $array[$i] = $array[$j];
                $array[$j] = $aux;
            }
        }
    }

    return $array;
}


$array = [3, 4, 2, 1];

echo "Array desordenado ";
for ($i=0;$i<count($array);$i++){
    echo $array[$i] . " ";
}


$array = arraySorted($array);


echo "<br/> Array ordenado ";

for ($i=0;$i<count($array);$i++){
    echo $array[$i] . " ";
}