<?php

$phrase = 'Hola mundo';

$reversePhrase = makeReverse($phrase);

echo 'Frase original: ' . $phrase . ' y frase invertida: ' . $reversePhrase;


function makeReverse($word){
    $newPhrase = '';
    for ($i = strlen($word); $i>=0; $i--){
        $newPhrase.=$word[$i];

    }

    return $newPhrase;
}