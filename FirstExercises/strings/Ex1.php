<?php

$word = 'Hola mundo';

$numberOfVowels = countVowels($word);

echo 'El número de vocales en la frase ' . $word . ' es de ' . $numberOfVowels;

function countVowels($vowels)
{
    $count = 0;
    for ($i = 0; $i <= strlen($vowels); $i++) {
        $vowel = $vowels[$i];
        $vowel = strtolower($vowel);
        if ($vowel == 'a' || $vowel == 'e' || $vowel == 'i' || $vowel == 'o' || $vowel == 'u') {
            $count++;
        }
    }

    return $count;
}