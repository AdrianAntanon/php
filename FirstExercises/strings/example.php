<?php

echo 'Strings PHP </br>';

$str = 'Hola mundo';
echo $str[0];

echo '</br>';

echo 'Longitud de la cadena' . strlen($str) . '</br>';
echo 'Número de palabras ' . str_word_count($str) . '</br>';
echo 'Inverso ' . strrev($str) . '</br>';
echo 'Buscar posición ' . strpos($str, "mundo") . '</br>';
echo 'Reemplazar ' . str_replace( "mundo", "planeta", $str) . '</br>';
echo 'Reemplazar con regex ' . preg_replace("/m[a-z]+/i", "planeta", $str) . '</br>';