<?php

$word = 'Hola mundo';

$numberOfConsonants = countConsonants($word);

echo 'El número de consonantes en la frase ' . $word . ' es de ' . $numberOfConsonants;

function countConsonants($consonants)
{
    $count = 0;
    for ($i = 0; $i <= strlen($consonants); $i++) {
        $consonant = $consonants[$i];
        $consonant = strtolower($consonant);
        if ($consonant != 'a' && $consonant != 'e' && $consonant != 'i' && $consonant != 'o' && $consonant != 'u' && $consonant != ' ') {
            $count++;
        }
    }

    return $count;
}